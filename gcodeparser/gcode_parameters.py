from gcodeparser import GcodeParser

class Gcode_parameters:

	def get_params(self, gcode:str=None, input_file_path:str=None,) -> (list, str):
		with open(input_file_path, 'r') as gcode_file:
			if gcode is not None:
				gcode_lines = gcode
			else:
				gcode_lines = gcode_file.read()

			pars = GcodeParser(gcode_lines)

			params_gcode = [(
							 ''.join(map(str, pars.lines[i].command)),
			                 0 if pars.lines[i].get_param('X') is None else pars.lines[i].get_param('X'),
			                 0 if pars.lines[i].get_param('Y') is None else pars.lines[i].get_param('Y'),
			                 0 if pars.lines[i].get_param('Z') is None else pars.lines[i].get_param('Z'),
			                 pars.lines[i].get_param('E'),
			                 float(pars.lines[i].comment) if len(pars.lines[i].comment) > 0 else 0,
			                 )
				for i in range(len(pars.lines))
			                ]

		return params_gcode, gcode_lines