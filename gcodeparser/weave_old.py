#!/usr/bin/python3
from gcode_split_layer import split_gcode
from add_missing_points import Missing_points
from add_in_gcode_turn_on_off_arc import add_arc_in_gcode_file

from progress.bar import FillingSquaresBar
import numpy as np
import re
import glob
import os
import sys
import json
from natsort import natsorted


class Weave(Missing_points):
	def __init__(self, gcode_path: str, weave_param: str, output_weave_folder_path: str ='/home/klipper/gcode_files/output_weave',
	             output_split_folder_path:str='/home/klipper/gcode_files/output_split'):

		self.gcode_file = None
		# self.gcode_path = f'/home/klipper/gcode_files/{gcode_path}'
		self.gcode_path = gcode_path
		self.weave_param = weave_param
		self.output_weave_folder_path = output_weave_folder_path
		self.output_split_folder_path = output_split_folder_path

		self.weave_pattern, self.weave_distance, \
		self.weave_amplitude, self.weave_angle, self.weave_pause_list = self.get_weave_param(self.weave_param)

		self.segments_length = self.weave_distance / len(self.weave_pattern)
		super().__init__(self.segments_length)

	def add_weave_all_layers(self):

		if not os.path.exists(self.output_weave_folder_path):
			os.makedirs(self.output_weave_folder_path)

		split_gcode(self.gcode_path, self.output_split_folder_path)  # исходный файл делиться по слоям

		files = glob.glob(f'{self.output_weave_folder_path}/*') # все файлы из папки output_weave_folder_path удаляются
		for f in files:
			os.remove(f)

		path_layer_list = natsorted(glob.glob(f'{self.output_split_folder_path}/layer *[0-9].gcode'))
		new_path = [path.replace(rf'{self.output_split_folder_path}', '') for path in path_layer_list]

		progress_bar = FillingSquaresBar('Add Weave', max=len(path_layer_list))
		with open(f'{self.output_weave_folder_path}/description.gcode', 'w') as description_weave, \
				open(f'{self.output_split_folder_path}/description.gcode', 'r') as description_split:
			description_weave.writelines(description_split.read())

			for gcode_file_index in range(len(path_layer_list)): # основной цикл weave
				progress_bar.next()

				with open(path_layer_list[gcode_file_index], 'r') as gcode_file:
					gcode_read = gcode_file.read()
					gcode_with_weave_points = self.add_weave_in_gcode(gcode_read, path_layer_list[gcode_file_index])
					description_split.seek(0)
					description_string = ''.join(i for i in description_split.readlines())
					gcode_with_weave_points.insert(0, description_string)
				with open(f'{self.output_weave_folder_path}/{new_path[gcode_file_index]}', 'w') as weave_file:
					weave_file.writelines(gcode_with_weave_points)

		with open(f'{self.output_weave_folder_path}/layer_end.gcode', 'w') as end_weave, \
			open(f'{self.output_split_folder_path}/layer_end.gcode', 'r') as end_split:

			end_weave.writelines(end_split.read())

		progress_bar.finish()

		# Для отладки, всё собирается в 1 файл#########################
		# p = 'output_weave'
		# if not os.path.exists('weave'):
		# 	os.makedirs('weave')
		#
		# path_layer_list = glob.glob(f'{p}/*.gcode')
		#
		# for i in range(len(path_layer_list)):
		# 	with open(f'output/description.gcode', 'a') as weave_file, open(f'{p}/layer {i + 1}.gcode') as layer:
		# 		weave_file.writelines(layer)
		#
		# with open(f'output/description.gcode', 'a') as weave_file, open(f'output/layer_end.gcode') as end_layer:
		# 	weave_file.writelines(end_layer)
		#
		# with open(f'output/description.gcode', 'r') as weave_file, \
		# 		open(f'weave/Weaved {self.gcode_path}', 'w') as fin:
		# 	fin.writelines(weave_file)
		###############################################################

		print('Add weave done!')

	def get_weave_param(self, param_name: str) -> (tuple, float or int, float or int, int):

		# with open('/home/klipper/xweld_projects/gcodeparser/weave_config.json', 'r') as json_file:
		# 	weave_param = json.load(json_file)
		with open('weave_config.json', 'r') as json_file:
			weave_param = json.load(json_file)

		return tuple(weave_param[f'{param_name}']['weave_pattern']), \
		       weave_param[f'{param_name}']['weave_distance'], \
		       weave_param[f'{param_name}']['weave_amplitude'], \
		       weave_param[f'{param_name}']['weave_angle'], \
		       [0, 0, 0, 0] if not 'weave_pause' in weave_param[f'{param_name}'] else \
			       weave_param[f'{param_name}']['weave_pause']

	def get_parameters_gcode(self, gcode: str = None, input_gcode_path: str = None) -> (list, str):
		return super().get_params_gcode(gcode, input_gcode_path)

	def add_missing_points(self, gcode, input_gcode_path: str = None):
		return super().add_missing_points_in_list(gcode, input_gcode_path)

	def add_weave_in_gcode(self, gcode, input_gcode_path: str = None) -> list:

		gcode_list = ''.join(self.add_missing_points(gcode, input_gcode_path))

		params, gcode_lines = self.get_parameters_gcode(gcode_list, input_gcode_path)

		list_with_weave = []
		pars_lines = gcode_lines.splitlines()
		i = 0

		for line in pars_lines:
			if i == len(params) - 1:
				list_with_weave.append(f'{line}\n')
				# list_with_weave.append(f'{pars_lines[-1]}\n') # если есть функция add_arc_in_gcode_file() в weave()
				break

			code_start = params[i][0]
			start = params[i][1], params[i][2], params[i][3]
			end = params[i + 1][1], params[i + 1][2], params[i + 1][3]
			start_extrusion = params[i][4]
			end_extrusion = params[i + 1][4]
			start_distance = params[i][5]

			start_end_extrusion_condition = (start_extrusion is None and end_extrusion is None) or \
			                                (start_extrusion is None and end_extrusion is not None) or \
			                                (start_extrusion is not None and end_extrusion is None)

			if re.findall(rf'{code_start}', line):

				if start_end_extrusion_condition:
					list_with_weave.append(f'{line}\n')
					i += 1
					continue
				elif start_extrusion > 0:

					line = line.partition('E')[0] + 'E' + str(start_extrusion) + '; ' + str(start_distance)

					self.weave_pause(list_with_weave, self.weave_pause_list[0])
					list_with_weave.append(f'{line}\n')
					self.weave_pause(list_with_weave, self.weave_pause_list[1])

					weave_p = self.weave_point(code_start, start, end, start_extrusion, end_extrusion,
					                           self.weave_amplitude, self.weave_angle)

					self.weave_pause(list_with_weave, self.weave_pause_list[2])
					list_with_weave.append(weave_p)
					self.weave_pause(list_with_weave, self.weave_pause_list[3])

				else:
					list_with_weave.append(f'{line}\n')
				i += 1
			else:
				list_with_weave.append(f'{line}\n')

		return list_with_weave

	def weave_pause(self, list_with_weave:list, weave_pause_seconds: int) -> list:
		list_with_weave.append(f'G4 P{weave_pause_seconds}\n') if weave_pause_seconds > 0 else None
		return list_with_weave


	def weave_point(self, code: str, start: tuple, end: tuple, start_extrusion: float, end_extrusion: float,
	                weave_amplitude: int, weave_angle: int) -> str:

		start_points = np.array(start)
		end_points = np.array(end)

		vector = end_points - start_points
		uni_vector = self.unit_vector(vector)
		uni_vector = self.z_rotation(uni_vector, weave_angle)
		uni_vector *= weave_amplitude
		weave_point = start_points + uni_vector
		weave_point_length = np.sqrt(uni_vector.dot(uni_vector))
		weave_extrusion = (start_extrusion + end_extrusion) / 2

		point = (f'{code}'
		         f'{" X" + str(round(weave_point[0], 4)) if weave_point[0] > weave_amplitude or weave_point[0] < -weave_amplitude else f" X{start[0]}" if start[0] > 0 else ""}'
		         f'{" Y" + str(round(weave_point[1], 4)) if weave_point[1] > weave_amplitude or weave_point[1] < -weave_amplitude else f" Y{start[1]}" if start[1] > 0 else ""}'
		         f'{" Z" + str(round(weave_point[2], 4)) if weave_point[2] > weave_amplitude or weave_point[2] < -weave_amplitude else ""}'
		         f'{" E" + str(round(weave_extrusion, 6))}; '
		         f'{(str(weave_point_length))}\n')
		return point

	def unit_vector(self, vector):
		""" Returns the unit vector of the vector."""
		return vector / np.sqrt(vector.dot(vector))

	# def angle_between(v1, v2):
	#     """Finds angle between two vectors"""
	#     v1_u = unit_vector(v1)
	#     v2_u = unit_vector(v2)
	#     return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
	#
	# def x_rotation(vector,theta):
	#     """Rotates 3-D vector around x-axis"""
	#     theta = (theta * np.pi) / 180
	#     R = np.array([[1,0,0],[0,np.cos(theta),-np.sin(theta)],[0, np.sin(theta), np.cos(theta)]])
	#     return np.dot(R,vector)
	#
	# def y_rotation(vector,theta):
	#     """Rotates 3-D vector around y-axis"""
	#     theta = (theta * np.pi) / 180
	#     R = np.array([[np.cos(theta),0,np.sin(theta)],[0,1,0],[-np.sin(theta), 0, np.cos(theta)]])
	#     return np.dot(R,vector)

	def z_rotation(self, vector, theta):
		"""Rotates 3-D vector around z-axis"""
		theta = (theta * np.pi) / 180
		R = np.array([[np.cos(theta), -np.sin(theta), 0], [np.sin(theta), np.cos(theta), 0], [0, 0, 1]])
		return np.dot(R, vector)


def call_pars_func(file_name='Calibration_cube.gcode', weave_param='weave_1', output_weave_folder_path: str =r'/home/klipper/gcode_files/output_weave',
	             output_split_folder_path:str=r'/home/klipper/gcode_files/output_split'):
	print(file_name, 'splitting...')
	Weave(file_name, weave_param, output_weave_folder_path, output_split_folder_path).add_weave_all_layers()


def main():
	if len(sys.argv) > 1:
		call_pars_func(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
	#
	# file_name = 'Calibration_cube.gcode'
	# weave_param = 'weave_2'
	# Weave(file_name, weave_param).add_weave_all_layers()

if __name__ == '__main__':
	main()
