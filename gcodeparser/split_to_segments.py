from gcode_parameters import Gcode_parameters

import math
import numpy as np
from gcodeparser import GcodeParser
import re
import glob
import os
import time
from progress.bar import FillingSquaresBar
"""
В каждом слое делит на сегменты, в зависимости от длины необходимых сегментов
"""
class Split_to_segments(Gcode_parameters):
	def __init__(self, segment_size):
		self.gcode = None
		self.segment_size = segment_size

	def split_to_segments_all_files(self, output_folder:str=None) -> None:

		path_layer_list = glob.glob(f'{output_folder}/layer *[0-9].gcode')

		new_path = [path.replace(rf'{output_folder}', '') for path in path_layer_list]
		bar = FillingSquaresBar('Split to segments', max=len(path_layer_list))
		for i in range(len(path_layer_list)):
			bar.next()
			with open(path_layer_list[i], 'r') as gcode_file:
				gcode_read = gcode_file.read()
			gcode_with_split_segments = self.add_split_segments_in_gcode(self.gcode, gcode_read)
			with open(f'{output_folder}/{new_path[i]}', 'w') as segments_file:
				segments_file.writelines(gcode_with_split_segments)
		bar.finish()


	def get_params_gcode(self, gcode:str=None, input_path:str=None) -> (list, str):
		return super().get_params(gcode, input_path)

	def add_split_segments_in_gcode(self, gcode:str=None, input_path:str=None) -> list:
		params, gcode_lines = self.get_params_gcode(gcode, input_path)

		list_with_split_segments = []
		pars_lines = gcode_lines.splitlines()
		i = 0
		# z_coord = None
		for line in pars_lines:
			if i == len(params) - 1:
				list_with_split_segments.append(f'{line}\n')
				break

			code_start = params[i][0]
			code_end = params[i + 1][0]
			start = params[i][1], params[i][2], params[i][3]
			end = params[i + 1][1], params[i + 1][2], params[i + 1][3]
			start_extrusion = params[i][4]
			end_extrusion = params[i + 1][4]
			start_distance = params[i][5]
			segments_length = math.floor(start_distance / self.segment_size)


			start_end_extrusion_condition = (start_extrusion is None and end_extrusion is None) or \
			                                (start_extrusion is None and end_extrusion is not None) or \
			                                (start_extrusion is not None and end_extrusion is None)

			# if 'LAYER' in line:
			# 	print(line, 'split')
				# list_with_split_segments.append(line)

			if re.findall(rf'{code_start}', line):

				if start_end_extrusion_condition:

					i += 1
					# if start[2] > 0 and z_coord is None:
					# 	z_coord = start[2]
					list_with_split_segments.append(f'{line}\n')
					# print(line)
					continue
				elif segments_length > 2 and code_start == ('G1') and\
						(start[0] > 0 and start[1] > 0 and end[0] > 0 and end[1] > 0):

					line = line.partition('E')[0] + ' E' + str(start_extrusion) + '; ' +  str(start_distance
					                                                                          -self.segment_size * segments_length)
					list_with_split_segments.append(f'{line}\n')


					split_points_list = self.split3d(code_end, start, end, segments_length,
					                                 start_extrusion, end_extrusion, self.segment_size)
					list_with_split_segments.extend(split_points_list)
				else:
					list_with_split_segments.append(f'{line}\n')
				i += 1
			else:
				list_with_split_segments.append(f'{line}\n')

		# with open('split_file.gcode' ,'w') as split:
		# 	split.writelines(list_with_split_segments)

		# print(list_with_split_segments[3])
		# print(list_with_split_segments)
		# print(list_with_split_segments)
		# list_with_split_segments = list_with_split_segments[:4] + ['M106 S255\n'] + list_with_split_segments[3:]
		# print(list_with_split_segments)
		return list_with_split_segments


	def split3d(self, code: str, start: tuple, end: tuple, segments_length: int, start_extrusion: float, end_extrusion:
	float, segment_size: float) -> list:
		x_delta = (end[0] - start[0]) / float(segments_length)
		y_delta = (end[1] - start[1]) / float(segments_length)
		z_delta = (end[2] - start[2]) / float(segments_length)
		e_delta = (end_extrusion - start_extrusion) / float(segments_length)

		points = []
		for i in range(1, segments_length):
			points.append(f'{code}'
			              f'{" X" + (str(round(start[0] + i * x_delta, 4))) if x_delta > 0 or x_delta < 0 else f" X{start[0]}" if start[0] > 0 else ""}'		              
			              f'{" Y" + (str(round(start[1] + i * y_delta, 4))) if y_delta > 0 or y_delta < 0 else f" Y{start[1]}" if start[1] > 0 else ""}'
			              # f'{" Z" + (str(round(start[2], 4))) if start[2] > 0 or start[2] < 0 else ""}'
			              f'{" E" + (str(round((start_extrusion + i * e_delta), 8))) if e_delta > 0 else ""}; '
			              f'{(str(segment_size))}\n')
		return points


def main():
	splits = Split_to_segments(0.5).split_to_segments_all_files('output')
	print(splits)

if __name__ == '__main__':
	main()
