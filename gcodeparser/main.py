import subprocess

from gcode_split_layer import split_gcode
from vector_length_gcode import Vector_lenght
from split_to_segments import Split_to_segments

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import transforms
from mpl_toolkits.mplot3d import Axes3D
from pandas import DataFrame
import re
import glob
import natsort
import os
from subprocess import PIPE, Popen
import json
def main():
    with open('weave_config.json', 'r') as j_file:
        file = json.load(j_file)

    print(file['weave_2']['weave_amplitude'])
if __name__ == '__main__':
    main()