from gcodeparser import GcodeParser
import re
import glob
import time
from progress.bar import FillingSquaresBar
from gcode_parameters import Gcode_parameters
from vector_length_gcode import Vector_lenght
from split_to_segments import Split_to_segments

class Missing_points(Split_to_segments, Vector_lenght):
	def __init__(self, segment_size, folder_split_output:str='output'):
		super().__init__(segment_size)
		self.segments_size = segment_size
		self.output_folder = folder_split_output
		self.first_string = ''

	def add_new_points_all_gcode(self, output_folder):
		path_layer_list = glob.glob(f'{output_folder}/layer *[0-9].gcode')

		new_path = [path.replace(rf'{output_folder}', '') for path in path_layer_list]

		bar = FillingSquaresBar('Add new points', max=len(path_layer_list))
		for i in range(len(path_layer_list)):
			bar.next()
			with open(f'{path_layer_list[i]}', 'r') as gcode_file:
				gcode = gcode_file.read()
				list_with_new_points = self.add_missing_points_in_list(gcode)

			with open(f'{output_folder}/{new_path[i]}', 'w') as segments_file:
				segments_file.writelines(list_with_new_points)
		bar.finish()


	def get_params_gcode(self, gcode:str=None, input_gcode_path:str=None) -> (list, str):
		return super().get_params_gcode(gcode, input_gcode_path)

	def calculate_gcode_distance(self, gcode:str=None, input_path:str= None) -> list:
		return super().calculate_gcode_distance(gcode, input_path)

	def calculate_distance_all_split_files(self, input_path:str=None) -> None:
		return super().calculate_distance_all_split_files(input_path)

	def add_split_segments_in_gcode(self, gcode:str=None, input_gcode_path:str=None) -> list:
		return super().add_split_segments_in_gcode(gcode, input_gcode_path)

	def split_to_segments_all_files(self, output_folder:str=None) -> None:
		return super().split_to_segments_all_files(output_folder)

	def add_missing_points_in_list(self, gcode:str=None, input_gcode_path:str=None, id=None):

		split_list = ''.join(self.calculate_gcode_distance(gcode, input_gcode_path))
		split_list = ''.join(self.add_split_segments_in_gcode(split_list, input_gcode_path))

		params, gcode_lines = self.get_params_gcode(split_list, input_gcode_path)
		# print(gcode_lines)
		pars_lines = gcode_lines.splitlines()
		list_with_new_points = []
		i = 0

		for line in (pars_lines):
			j = i - 1
			k = j
			if i == len(params) - 1:
				list_with_new_points.append(f'{line}\n')
				break
			code_start = params[i][0]
			start = params[i][1], params[i][2], params[i][3]
			end = params[j][1], params[j][2], params[j][3]
			start_extrusion = 0 if params[i][4] is None else params[i][4]
			end_extrusion = params[i-1][4]
			prev_distance = params[j][5]
			x_diff, y_diff, z_diff = abs(start[0] - end[0]), abs(start[1] - end[1]), abs(start[2] - end[2])
			diff_num = self.segments_size

			if re.findall(rf'{code_start}', f'{line}\n'):
				if code_start.startswith('G') and start_extrusion > 0:

					while prev_distance > diff_num and j != i - 5 and j > 0:
						diff_condition = (x_diff > diff_num or y_diff > diff_num or z_diff > diff_num)
						start_end_cond = (start[0] > 0 or start[1] > 0) and (end[0] > 0 or end[1] > 0)

						if diff_condition and start_end_cond:
							while end_extrusion is None and k != i - 5 and k > 0:
								k -= 1
								end_extrusion = params[k][4]
							if end_extrusion is None:
								end_extrusion = start_extrusion

							list_with_new_points.append(f'{code_start}'
							f'{" X" + str(end[0]) if end[0] > 0 or end[0] < 0 else ""}'
			                f'{" Y" + str(end[1]) if end[1] > 0 or end[1] < 0 else ""}'
			                f'{" E" + str(end_extrusion) if end_extrusion > 0 else f" E{start_extrusion-2}"}\n')
							# list_with_new_points.append(f'{line}\n')
							# print(end_extrusion)
							break
						else:
							j -= 1
							end = params[j][1], params[j][2], params[j][3]
							x_diff = abs(start[0] - end[0])
							y_diff = abs(start[1] - end[1])
							z_diff = abs(start[2] - end[2])
					list_with_new_points.append(f'{line}\n')
				else:
					list_with_new_points.append(f'{line}\n')
				i += 1
			else:
				list_with_new_points.append(f'{line}\n')


		gcode_with_mis_points = ''.join(list_with_new_points)
		split_list = self.calculate_gcode_distance(gcode_with_mis_points, input_gcode_path)
		gcode_dist = ''.join(split_list)
		split_list = ''.join(self.add_split_segments_in_gcode(gcode_dist, input_gcode_path))

		with open(f'split_files/{id[14:]}', 'w') as spl:
			spl.writelines(split_list)

		return split_list

def main():
	# new_points = Missing_points(0.5).add_missing_points_in_list(input_gcode_path='output/layer 2.gcode')
	# print(new_points)

	path = 'output_split/layer 50.gcode'
	split_list = Missing_points(0.375).add_missing_points_in_list(input_gcode_path=path)
	with open(f'split_files/50.gcode', 'w') as spl:
		spl.writelines(split_list)

	# print(new_points)


if __name__=='__main__':
	main()

