## !/usr/bin/python
import re
from collections import deque
import os
import glob
import sys
import stat
from gcodeparser import GcodeParser
import math
import heapq
from vector_length_gcode import Vector_lenght
'''
Делит исходный gcode файл по слоям в папку {output_path}
'''

def split_gcode(path: str, output_path:str) -> None:

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    files = glob.glob(f'{output_path}/*')
    for f in files:
        os.chmod(f, stat.S_IWRITE)
        os.remove(f)

    with open(path, 'r') as gcode_file:
        lines = deque()
        lines.extend(gcode_file.readlines())
        # lines.insert(3, f'G92 E0.0000\n')
        pars_description(lines, output_path)
        pars_end_layer(lines, output_path)
        pars_layer_gcode(lines, output_path)

        # p = 'output_split'
        # if not os.path.exists('weave'):
        #     os.makedirs('weave')
        #
        # path_layer_list = glob.glob(f'{p}/*.gcode')
        #
        # for i in range(len(path_layer_list) - 2):
        #     with open(f'{p}/description.gcode', 'a') as weave_file, open(f'{p}/layer {i + 1}.gcode') as layer:
        #         weave_file.writelines(layer)
        #
        # with open(f'{p}/description.gcode', 'a') as weave_file, \
        #         open(f'output_split/layer_end.gcode') as end_layer:
        #     weave_file.writelines(end_layer)
        #
        # with open(f'{p}/description.gcode', 'r') as weave_file, \
        #         open(f'weave/splot_1.gcode', 'w') as fin:
        #     fin.writelines(weave_file)

    print('Gcode file is layered!')

def pars_description(gcode_lines:deque, output_path:str) -> None:

    pattern = re.compile(r'\blayer 1\b|\bLAYER:0\b')

    with open(f'{output_path}/description.gcode', 'w') as description_file:
        while not pattern.findall(gcode_lines[0]):
            description_file.write(gcode_lines.popleft())


def pars_end_layer(gcode_lines:deque, output_path:str) -> None:
    with open(f'{output_path}/layer_end.gcode', 'w') as end_layer_file:
        end_list = []

        pattern = re.compile(r'\blayer end\b|\bEnd of Gcode\b')
        while not pattern.findall(gcode_lines[-1]):
            end_list.append(gcode_lines.pop())
        else:
            end_list.append(gcode_lines.pop())
            end_list.reverse()

        end_layer_file.writelines(end_list)

def layer_counter(gcode_lines:deque) -> int:
    count = 0
    pattern = re.compile(r'\blayer \d+\b|\bLAYER:\d+\b')
    for i in range(len(gcode_lines)):
        if pattern.findall(gcode_lines[i]):
            count += 1

    return count



def pars_layer_gcode(gcode_lines:deque, output_path:str) -> None:

    split_layer_del_comm = del_comm(list(gcode_lines))
    # print(split_layer_del_comm[:5])
    gcode_lines = deque()
    gcode_lines.extend(split_layer_del_comm)

    split_layer = splitting_layer_gcode(gcode_lines)
    add_g0_to_prev_layer(split_layer)
    split_layer = length_between_g1(split_layer)

    # layers = extrusion_conversion(split_layer)
    for i in range(len(split_layer)):
        with open(rf'{output_path}/layer {i + 1}.gcode', 'w') as layer_file:

            # del_comm_layer, layer_name = del_comm(split_layer[i])
            # del_comm_layer.insert(4, layer_name)
            layer_file.writelines(split_layer[i])


def splitting_layer_gcode(gcode_lines:deque) -> list:
    split_layer = [[] for i in range(layer_counter(gcode_lines))]

    j, i = 1, 0

    while len(gcode_lines) > 0:
        if 'G0\nG0\n' not in split_layer[i]:
            split_layer[i].append('G0\nG0\n')

        first_pattern = re.compile(rf'\blayer \d+\b|\bLAYER:0\b|\bNONMESH\b')
        # second_pattern = re.compile(rf'\blayer {j}\b')

        if first_pattern.findall(gcode_lines[0]):
            # print(gcode_lines[0]) if f'LAYER:{j}' in gcode_lines[0] else None
            split_layer[i].append(gcode_lines.popleft())

            while  len(gcode_lines)> 0 and f';LAYER:{j}' not in gcode_lines[0]:
                # print(len(gcode_lines))
                split_layer[i].append(gcode_lines.popleft())

        else:
            while len(gcode_lines) > 0 and f';LAYER:{j}' not in gcode_lines[0]:

                if first_pattern.findall(gcode_lines[0]):
                    # print(gcode_lines[1])
                    break
                else:
                    split_layer[i].append(gcode_lines.popleft())

            j += 1
            i += 1


    return split_layer

def add_g0_to_prev_layer(split_layer:list):

    for i in range(1, len(split_layer)):

        layer = ''.join(split_layer[i])
        layer_previous = ''.join(split_layer[i-1])

        pars = GcodeParser(layer)
        params_gcode_layer = [
            (
                ''.join(map(str, pars.lines[param_idx].command)),
                pars.lines[param_idx].get_param('Z'),
                pars.lines[param_idx].get_param('E') if pars.lines[param_idx].get_param('E') is not None else 0
                )
            for param_idx in range(len(pars.lines))
            ]

        pars_previous = GcodeParser(layer_previous)
        params_gcode_layer_previous = [
            (
                ''.join(map(str, pars_previous.lines[param_idx].command)),
                pars_previous.lines[param_idx].get_param('Z'),
                pars_previous.lines[param_idx].get_param('E') if pars_previous.lines[param_idx].get_param('E') is not None else 0
                )
            for param_idx in range(len(pars_previous.lines))
            ]

        extrusion_bolshe_0 = list(filter(lambda x:x[2] > 0, params_gcode_layer))[0][2]
        z_bolshe_0_current = list(filter(lambda x: x[1] is not None, params_gcode_layer))


        z_bolshe_0_previous = list(filter(lambda x: x[1] is not None, params_gcode_layer_previous))
        extrusion_bolshe_0_previous_layer = list(filter(lambda x: x[2] > 0, params_gcode_layer_previous))[0][2]


        # if 'LAYER:' in split_layer[i-1][1]:
        if i - 1 == 0:
             # добавить в 0 слой G92 и дополнительную точку
            split_layer[0][3] = 'G92 E0\n'
            g1 = 'G1' + split_layer[0][2].partition('F9000')[2].partition('\n')[0] + ' E' + str(
                extrusion_bolshe_0_previous_layer - 1.5) + '\n'
            split_layer[0].insert(4, g1)
        elif i - 1 > 0 and len(z_bolshe_0_previous) == 1:
            z_idx = params_gcode_layer.index(z_bolshe_0_current[0])
            g0_str = split_layer[i][z_idx]
            g1 = 'G1' + g0_str.partition('F9000')[2].partition('Z')[0] + 'E' + str(
                extrusion_bolshe_0 - 1.5) + '\n'
            split_layer[i].insert(3, g1)



        # print(len(z_bolshe_0_previous), i, z_bolshe_0_previous)

        if len(z_bolshe_0_previous) > 1:
            print(i)
            if len(z_bolshe_0_current) > 0:
                z_idx = params_gcode_layer.index(z_bolshe_0_current[0])
                if z_idx < 10:
                    last_z_value = params_gcode_layer[z_idx][1]
                    g0_str = split_layer[i].pop(z_idx)
                else:
                    last_z_value = z_bolshe_0_previous[-1][1]
                    g0_str = split_layer[i - 1][-1].partition('\n')[0] + f" Z{last_z_value}" + '\n'
            else:
                last_z_value = z_bolshe_0_previous[-1][1]
                g0_str = split_layer[i - 1][-1].partition('\n')[0] + f" Z{last_z_value}" + '\n'
            # split_layer_line_z = list(filter(lambda x: 'Z' + str(last_z_value) in x, split_layer[i-1]))
            # gcode_index_z = split_layer[i-1].index(split_layer_line_z[0])
            # g0_str = split_layer[i - 1][-1].partition('\n')[0] + f" Z{last_z_value}" + '\n'

            if 'G1' in g0_str:
                g0_str = 'G0' + g0_str.partition('G1')[2].partition('E')[0] \
                          + f"Z{last_z_value}" + '\n'

            split_layer[i].insert(0, g0_str)
            # print(g0_str)
            if 'F9000' in g0_str:
                g1_part = 'G1' + g0_str.partition('F9000')[2].partition('Z')[0] + 'E' + str(extrusion_bolshe_0)\
                          + '\n'
            elif 'F300' in g0_str:
                g1_part = 'G1' + g0_str.partition('F300')[2].partition('Z')[0] + 'E' + str(
                    extrusion_bolshe_0) + '\n'
            else:
                g1_part = 'G1' + g0_str.partition('G0')[2].partition('Z')[0] + 'E' + str(
                        extrusion_bolshe_0) + '\n'

            # print(g1_part)
            # print(g1_part)
            # print(len(z_bolshe_0), 'len')

            # print(i)
            split_layer[i].insert(3, g1_part)
            split_layer[i].insert(2, f'G92 E{extrusion_bolshe_0}\n')


def get_gcode_params(split_layer:list)-> list:
    params_layer = []
    for gcode_layer in split_layer:

        gcode_layer = ''.join(gcode_layer)

        layer = ''.join(gcode_layer)
        pars = GcodeParser(layer)

        params_gcode_layer = [
            (
                ''.join(map(str, pars.lines[i].command)),
                0 if pars.lines[i].get_param('X') is None else pars.lines[i].get_param('X'),
                0 if pars.lines[i].get_param('Y') is None else pars.lines[i].get_param('Y'),
                0 if pars.lines[i].get_param('Z') is None else pars.lines[i].get_param('Z'),
                pars.lines[i].get_param('E') if pars.lines[i].get_param('E') is not None else 0,
                pars.lines[i].comment
                )
            for i in range(len(pars.lines))
            ]

        params_layer.append(params_gcode_layer)

    return params_layer


def length_between_g1(split_layer:list):
    gcode_params = get_gcode_params(split_layer)
    length_between_g1_list = [[] for li in range(len(split_layer))]

    layer = 0
    while layer < len(split_layer) - 1:
        i = 0
        j = i + 1
        for line in split_layer[layer]:
            if len(line) > 0:
                if i == len(split_layer[layer]) - 1:
                    length_between_g1_list[layer].append(line)
                    layer += 1
                    i = 0
                    j = i + 1
                    continue
                elif j == len(split_layer[layer]) - 1:
                    length_between_g1_list[layer].append(line)
                    length_between_g1_list[layer].append(split_layer[layer][-1])
                    layer += 1
                    i = 0
                    j = i + 1
                    continue
                code_start = gcode_params[layer][i][0]
                code_end = gcode_params[layer][j][0]
                start_coord = gcode_params[layer][i][1], gcode_params[layer][i][2], gcode_params[layer][i][3]
                end_coord = gcode_params[layer][j][1], gcode_params[layer][j][2], gcode_params[layer][j][3]
                start_extrusion = gcode_params[layer][i][4]
                end_extrusion = gcode_params[layer][j][4]

                if code_start in line:
                    if code_start == 'G1' and i < len(gcode_params) - 1:
                        while code_end != 'G1' and j < len(split_layer[layer]) - 1:
                            j += 1
                            code_end = gcode_params[layer][j][0]
                            end_coord = gcode_params[layer][j][1], gcode_params[layer][j][2], gcode_params[layer][j][3]
                            end_extrusion = gcode_params[layer][j][4]

                        if code_start == 'G1' and code_end == 'G1':
                            dist = ((end_coord[0] - start_coord[0]) ** 2 +
                                    (end_coord[1] - start_coord[1]) ** 2 +
                                    (end_coord[1] - start_coord[1]) ** 2) ** 0.5

                            length_between_g1_list[layer].append((line.partition('\n')[0] + '; ' + str(dist) + '\n'))
                            i += 1
                            j = i + 1
                            continue
                    else:
                        length_between_g1_list[layer].append(line)
                        i += 1
                        j = i + 1
                        continue
                else:
                    i += 1
                    j = i + 1
                    length_between_g1_list[layer].append(line)


    return length_between_g1_list


def extrusion_condevstaion():
    g92_string = len(list(filter(lambda x: x[0] == 'G92', params_gcode_layer)))
    last_extrusion = list(filter(lambda x: x[0] == 'G1', params_gcode_layer))[-1][-1]
    if g92_string > 0:
        largest_extrusion = heapq.nlargest(g92_string, params_gcode_layer, key=lambda x: x[4])
        sum_extrusion = sum(map(lambda x: x[4], largest_extrusion))
        print(sum_extrusion)
    print(last_extrusion)
    print()

def append_extr(gcode_layer, params):

    i = 0
    layer_extr_conversion = []
    first_extrusion = None
    z_coord = None

    for line in gcode_layer:
        code_start = params[i][0]
        start_coord = params[i][1], params[i][2], params[i][3]
        start_extrusion = params[i][4]

        if i == len(params) - 1:
            if z_coord is not None:
                n_point = new_point(code_start, start_coord, start_extrusion, first_extrusion, z_coord)
                layer_extr_conversion.append(n_point)
            break

        start_end_extrusion_condition = start_extrusion is None


        if re.findall(rf'{code_start}', line):
            if start_coord[2] > 0 and z_coord is None:
                z_coord = start_coord[2]

            if start_end_extrusion_condition:

                # if start_coord[2] > 0 or start_coord[1] > 0:
                #     line = line.partition('G0')[0] + 'G0' + ' F1500' + ' X'+ str(start_coord[0])\
                #            + ' Y' + str(start_coord[1]) + ' Z' + str(z_coord) + '\n'

                layer_extr_conversion.append(line)
                i += 1
                continue
            elif start_extrusion > 0:

                if first_extrusion is None:
                    first_extrusion = start_extrusion

                n_point = new_point(code_start, start_coord, start_extrusion, first_extrusion, z_coord)
                layer_extr_conversion.append(n_point)
            else:
                if start_coord[2] > 0 and start_coord[1] > 0:
                    line = line.partition('\n')[0] + ' Y' + str(start_coord[1]) + ' Z' + str(z_coord) + '\n'

                layer_extr_conversion.append(line)
            i += 1
        else:
            layer_extr_conversion.append(line)

    return layer_extr_conversion


def new_point(code: str, coord: tuple, extrusion, first_extrusion, z_coord) -> (str, float):
    if extrusion is None:
        extrusion = 0
    extrusion = extrusion
    # extrusion = extrusion - (int(first_extrusion) - 2.8)

    # print(coord)
    point = (
             f'{code}'
             f'{" X" + str(round(coord[0], 4)) if coord[0] > 0 else ""}'
             f'{" Y" + str(round(coord[1], 4)) if coord[1] > 0 else ""}'
             # f'{" Z" + str(round(z_coord, 4)) if z_coord is not None and z_coord > 0 else ""}'
            f'{" Z" + str(round(z_coord, 4)) if z_coord > 0 else ""}'
             f'{" E" + str(round(extrusion, 6))}\n'
             )
    return point

def del_comm(gcode_splitlines: list) -> (list, str):
    first_pattern = re.compile(r'\bLAYER:\d+\b')

    for i in range(len(gcode_splitlines)):
        if first_pattern.findall(gcode_splitlines[i]):
            # layer_name = gcode_splitlines[i]
            # gcode_splitlines[i] = gcode_splitlines[i].split(';')[0]
            continue
        elif 'M106' in gcode_splitlines[i] or 'M107' in gcode_splitlines[i]:
            gcode_splitlines[i] = ''
        elif ';' in gcode_splitlines[i]:
            gcode_splitlines[i] = gcode_splitlines[i].split(';')[0]
    list_del_com = list(filter(len, gcode_splitlines))

    return list_del_com

def main():
    file_name = 'Calibration_cube.gcode'
    split_gcode(file_name, r'D:\Projects\xweld_projects\gcodeparser\output_split')

if __name__ == "__main__":
    main()
