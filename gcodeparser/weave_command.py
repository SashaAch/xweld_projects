import os, logging
import subprocess
from glob import glob
from natsort import natsorted
import time
from kinematics import extruder
import json
import sys

VALID_GCODE_EXTS = ['gcode', 'g', 'gco']


class Weave_Command:
	def __init__(self, config):
		self.printer = config.get_printer()

		# sdcard state
		# self.extruder_stepper = extruder.PrinterExtruder(config, 1)
		sensor_type = None
		sd = config.get('path')
		self.sdcard_dirname = os.path.normpath(os.path.expanduser(sd))
		self.current_file = None
		self.file_position = self.file_size = 0
		# Print Stat Tracking
		self.print_stats = self.printer.load_object(config, 'print_stats')
		# Work timer
		self.reactor = self.printer.get_reactor()
		self.must_pause_work = self.cmd_from_sd = False
		self.next_file_position = 0
		self.work_timer = None

		# print_index
		self.layer_index = 0

		self.split_path = "/home/klipper/printer_data/gcodes/output_split/*"
		self.weave_path = "/home/klipper/printer_data/gcodes/output_weave/*"

		self.split_path_part = self.split_path.partition('output_split')[1]
		self.weave_path_part = self.weave_path.partition('output_weave')[1]

		# Error handling
		gcode_macro = self.printer.load_object(config, 'gcode_macro')
		self.on_error_gcode = gcode_macro.load_template(
			config, 'on_error_gcode', '')
		# Register commands
		self.gcode = self.printer.lookup_object('gcode')

		self.gcode.register_command(
			"WEAVE_FILE", self.cmd_SDCARD_WEAVE_FILE, desc=self.cmd_SDCARD_PRINT_FILE_help)
		self.gcode.register_command(
			"PRINT_SPLIT_FILE", self.cmd_PRINT_SPLIT_FILE)
		self.gcode.register_command(
			"PRINT_WEAVE_FILE", self.cmd_PRINT_WEAVE_FILE)
		self.gcode.register_command(
			'REPEAT_SPLIT_LAYER', self.cmd_REPEAT_SPLIT_LAYER
			)
		self.gcode.register_command(
			'REPEAT_WEAVE_LAYER', self.cmd_REPEAT_WEAVE_LAYER
			)
		self.gcode.register_command(
			'NEXT_SPLIT_LAYER', self.cmd_NEXT_SPLIT_LAYER
			)
		self.gcode.register_command(
			'NEXT_WEAVE_LAYER', self.cmd_NEXT_WEAVE_LAYER
			)
		self.gcode.register_command(
			'SKIP_SPLIT_LAYER', self.cmd_SKIP_SPLIT_LAYER
			)
		self.gcode.register_command(
			'SKIP_WEAVE_LAYER', self.cmd_SKIP_WEAVE_LAYER
			)

	cmd_SDCARD_PRINT_FILE_help = "Loads a SD file and starts the print.  May " \
	                             "include files in subdirectories."

	def cmd_SDCARD_WEAVE_FILE(self, gcmd):

		# gcmd.respond_raw("sys: %s" % (new_split_file_list))

		self.layer_index = 0

		if self.work_timer is not None:
			raise gcmd.error("SD busy")
		self._reset_file()
		filename = '/home/klipper/printer_data/gcodes/' + gcmd.get("FILENAME")

		weave_param_name = gcmd.get("WEAVE_PARAM_NAME")

		if weave_param_name is not None:
			with open('/home/klipper/xweld_projects/gcode_parser/weave_config.json') as j_file:
				file = json.load(j_file)
				nozzle_diameter = file[weave_param_name]['weave_amplitude']

			self.gcode.run_script_from_command("SET_EXTRUDER NOZZLE_DIAMETER='{0}'".format(nozzle_diameter))

			subprocess.call(
				f"sh /home/klipper/xweld_projects/gcode_parser/call_weave.sh {filename} {weave_param_name}", shell=True)

	# self.do_resume()

	def cmd_PRINT_SPLIT_FILE(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("Идёт печать, подождите её окончания")
		self._reset_file()

		self.layer_index = int(gcmd.get("LAYER"))
		split_file_list = natsorted(glob(self.split_path))

		new_split_file_list = []
		for i in split_file_list:
			new_split_file_list.append(f"{self.split_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_split_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_PRINT_WEAVE_FILE(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("SD busy")
		self._reset_file()
		self.layer_index = int(gcmd.get("LAYER"))
		weave_file_list = natsorted(glob(self.weave_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.weave_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	# file_ind = 0
	# посмтреть с progress
	# while file_ind != len(new_weave_file_list):
	# 	file_stats = self.get_status()
	#
	# 	if file_stats['is_active']:
	# 		gcmd.respond_raw("is_active: %s" % (file_stats['is_active']))
	# 		continue
	# 	else:
	# 		self.do_resume()
	# 		self._load_file(gcmd, new_weave_file_list[file_ind], check_subdirs=True)
	# 		file_ind += 1

	def cmd_REPEAT_SPLIT_LAYER(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("Идёт печать, подождите её окончания")
		self._reset_file()

		new_split_file_list = natsorted(glob(self.split_path))
		new_new_split_file_list = []
		for i in new_split_file_list:
			new_new_split_file_list.append(f"{self.split_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_new_split_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_REPEAT_WEAVE_LAYER(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("Идёт печать, подождите её окончания")
		self._reset_file()

		weave_file_list = natsorted(glob(self.weave_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.weave_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_NEXT_SPLIT_LAYER(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("Идёт печать, подождите её окончания")
		self._reset_file()

		self.layer_index += 1
		weave_file_list = natsorted(glob(self.split_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.split_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_NEXT_WEAVE_LAYER(self, gcmd):
		if self.work_timer is not None:
			raise gcmd.error("Идёт печать, подождите её окончания")
		self._reset_file()

		self.layer_index += 1
		weave_file_list = natsorted(glob(self.weave_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.weave_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_SKIP_SPLIT_LAYER(self, gcmd):
		if self.work_timer is not None:
			self.gcode.run_script_from_command("CANCEL_PRINT")

		self._reset_file()
		self.layer_index += 1
		weave_file_list = natsorted(glob(self.split_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.split_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def cmd_SKIP_WEAVE_LAYER(self, gcmd):
		if self.work_timer is not None:
			self.gcode.run_script_from_command("CANCEL_PRINT")

		self._reset_file()
		self.layer_index += 1
		weave_file_list = natsorted(glob(self.weave_path))
		new_weave_file_list = []
		for i in weave_file_list:
			new_weave_file_list.append(f"{self.weave_path_part}/{os.path.split(i)[1]}")

		self._load_file(gcmd, new_weave_file_list[self.layer_index], check_subdirs=True)
		self.do_resume()

	def get_status(self, eventtime=0):
		return {
			'file_path': self.file_path(),
			'progress': self.progress(),
			'is_active': self.is_active(),
			'file_position': self.file_position,
			'file_size': self.file_size,
			}

	def file_path(self):
		if self.current_file:
			return self.current_file.name
		return None

	def progress(self):
		if self.file_size:
			return float(self.file_position) / self.file_size
		else:
			return 0.

	def is_active(self):
		return self.work_timer is not None

	def _reset_file(self):
		if self.current_file is not None:
			self.do_pause()
			self.current_file.close()
			self.current_file = None
		self.file_position = self.file_size = 0.
		self.print_stats.reset()
		self.printer.send_event("virtual_sdcard:reset_file")

	cmd_SDCARD_RESET_FILE_help = "Clears a loaded SD File. Stops the print " \
	                             "if necessary"

	def _load_file(self, gcmd, filename, check_subdirs=False):
		files = self.get_file_list(check_subdirs)
		flist = [f[0] for f in files]
		files_by_lower = {fname.lower(): fname for fname, fsize in files}
		fname = filename
		try:
			if fname not in flist:
				fname = files_by_lower[fname.lower()]
			fname = os.path.join(self.sdcard_dirname, fname)
			f = open(fname, 'r')
			f.seek(0, os.SEEK_END)
			fsize = f.tell()
			f.seek(0)
		except:
			logging.exception("virtual_sdcard file open")
			raise gcmd.error("Unable to open file")
		gcmd.respond_raw("File opened:%s Size:%d" % (filename, fsize))
		gcmd.respond_raw("File selected")
		self.current_file = f
		self.file_position = 0
		self.file_size = fsize
		self.print_stats.set_current_file(filename)

	def do_resume(self):
		if self.work_timer is not None:
			raise self.gcode.error("SD busy")
		self.must_pause_work = False
		self.work_timer = self.reactor.register_timer(
			self.work_handler, self.reactor.NOW)

	def do_pause(self):
		if self.work_timer is not None:
			self.must_pause_work = True
			while self.work_timer is not None and not self.cmd_from_sd:
				self.reactor.pause(self.reactor.monotonic() + .001)

	def get_file_list(self, check_subdirs=False):
		if check_subdirs:
			flist = []
			for root, dirs, files in os.walk(
					self.sdcard_dirname, followlinks=True):
				for name in files:
					ext = name[name.rfind('.') + 1:]
					if ext not in VALID_GCODE_EXTS:
						continue
					full_path = os.path.join(root, name)
					r_path = full_path[len(self.sdcard_dirname) + 1:]
					size = os.path.getsize(full_path)
					flist.append((r_path, size))
			return sorted(flist, key=lambda f: f[0].lower())
		else:
			dname = self.sdcard_dirname
			try:
				filenames = os.listdir(self.sdcard_dirname)
				return [(fname, os.path.getsize(os.path.join(dname, fname)))
				        for fname in sorted(filenames, key=str.lower)
				        if not fname.startswith('.')
				        and os.path.isfile((os.path.join(dname, fname)))]
			except:
				logging.exception("virtual_sdcard get_file_list")
				raise self.gcode.error("Unable to get file list")

	def work_handler(self, eventtime):
		logging.info("Starting SD card print (position %d)", self.file_position)
		self.reactor.unregister_timer(self.work_timer)
		try:
			self.current_file.seek(self.file_position)
		except:
			logging.exception("virtual_sdcard seek")
			self.work_timer = None
			return self.reactor.NEVER
		self.print_stats.note_start()
		gcode_mutex = self.gcode.get_mutex()
		partial_input = ""
		lines = []
		error_message = None
		while not self.must_pause_work:
			if not lines:
				# Read more data
				try:
					data = self.current_file.read(8192)
				except:
					logging.exception("virtual_sdcard read")
					break
				if not data:
					# End of file
					self.current_file.close()
					self.current_file = None
					logging.info("Finished SD card print")
					self.gcode.respond_raw("Done printing file")
					break
				lines = data.split('\n')
				lines[0] = partial_input + lines[0]
				partial_input = lines.pop()
				lines.reverse()
				self.reactor.pause(self.reactor.NOW)
				continue
			# Pause if any other request is pending in the gcode class
			if gcode_mutex.test():
				self.reactor.pause(self.reactor.monotonic() + 0.100)
				continue
			# Dispatch command
			self.cmd_from_sd = True
			line = lines.pop()
			next_file_position = self.file_position + len(line) + 1
			self.next_file_position = next_file_position
			try:
				self.gcode.run_script(line)
			except self.gcode.error as e:
				error_message = str(e)
				try:
					self.gcode.run_script(self.on_error_gcode.render())
				except:
					logging.exception("virtual_sdcard on_error")
				break
			except:
				logging.exception("virtual_sdcard dispatch")
				break
			self.cmd_from_sd = False
			self.file_position = self.next_file_position
			# Do we need to skip around?
			if self.next_file_position != next_file_position:
				try:
					self.current_file.seek(self.file_position)
				except:
					logging.exception("virtual_sdcard seek")
					self.work_timer = None
					return self.reactor.NEVER
				lines = []
				partial_input = ""
		logging.info("Exiting SD card print (position %d)", self.file_position)
		self.work_timer = None
		self.cmd_from_sd = False
		if error_message is not None:
			self.print_stats.note_error(error_message)
		elif self.current_file is not None:
			self.print_stats.note_pause()
		else:
			self.print_stats.note_complete()
		return self.reactor.NEVER


def load_config(config):
	return Weave_Command(config)