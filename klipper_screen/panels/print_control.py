import gi
import logging
import contextlib

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from ks_includes.screen_panel import ScreenPanel
from ks_includes.widgets.graph import HeaterGraph
from ks_includes.widgets.keypad import Keypad


def create_panel(*args):
    return PrintControl(*args)


class PrintControl(ScreenPanel):

    def __init__(self, screen, title, back=True):
        super().__init__(screen, title, back)
        self.settings = {}
        self.menu = ['move_menu']

    def initialize(self, panel_name):
        grid = self._gtk.HomogeneousGrid()

        self.labels['next_split'] = self._gtk.ButtonImage("arrow-up", "Следующий слой (обычный)", "color4")
        self.labels['next_split'].connect("clicked", self.run_gcode_macro, "NEXT_SPLIT_LAYER")

        self.labels['next_weave'] = self._gtk.ButtonImage("arrow-up", "Следующий слой (вив)", "color4")
        self.labels['next_weave'].connect("clicked", self.run_gcode_macro, "NEXT_WEAVE_LAYER")

        self.labels['repeat_split'] = self._gtk.ButtonImage("arrow-up", "Повторить слой (обычный)", "color4")
        self.labels['repeat_split'].connect("clicked", self.run_gcode_macro, "REPEAT_SPLIT_LAYER")

        self.labels['repeat_weave'] = self._gtk.ButtonImage("arrow-up", "Повторить слой (вив)", "color4")
        self.labels['repeat_weave'].connect("clicked", self.run_gcode_macro, "REPEAT_WEAVE_LAYER")

        self.labels['skip_split'] = self._gtk.ButtonImage("arrow-up", "Пропустить слой (обычный)", "color4")
        self.labels['skip_split'].connect("clicked", self.run_gcode_macro, "SKIP_SPLIT_LAYER")

        self.labels['skip_weave'] = self._gtk.ButtonImage("arrow-up", "Пропустить слой (вив)", "color4")
        self.labels['skip_weave'].connect("clicked", self.run_gcode_macro, "SKIP_WEAVE_LAYER")

        grid.attach(self.labels['skip_weave'], 2, 1, 1, 1)
        grid.attach(self.labels['next_weave'], 0, 1, 1, 1)
        # grid.attach(self.labels['feed-forward'], 0, 1, 1, 1)
        # grid.attach(self.labels['gastest'], 2, 1, 1, 1)
        grid.attach(self.labels['repeat_split'], 1, 0, 1, 1)
        grid.attach(self.labels['repeat_weave'], 1, 1, 1, 1)

        grid.attach(self.labels['next_split'], 0, 0, 1, 1)
        grid.attach(self.labels['skip_split'], 2, 0, 1, 1)

        self.labels['move_menu'] = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.labels['move_menu'].set_vexpand(True)
        self.labels['move_menu'].pack_start(grid, True, True, 0)


        self.content.add(self.labels['move_menu'])


    def process_update(self, action, data):
        if action != "notify_status_update":
            return

        # homed_axes = self._screen.printer.get_stat("toolhead", "homed_axes")
        # if homed_axes == "xyz":
        #    if "gcode_move" in data and "gcode_position" in data["gcode_move"]:
        #        self.labels['pos_x'].set_text(f"X: {data['gcode_move']['gcode_position'][0]:.2f}")

    def run_gcode_macro(self, widget, macro):
        self._screen._ws.klippy.gcode_script(macro)

    def back(self):
        if len(self.menu) > 1:
            self.unload_menu()
            return True
        return False
