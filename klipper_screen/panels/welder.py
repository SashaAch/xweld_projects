import gi
import logging

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango

from ks_includes.KlippyGcodes import KlippyGcodes
from ks_includes.screen_panel import ScreenPanel

AXIS_X = "X"
AXIS_Y = "Y"
AXIS_Z = "Z"


def create_panel(*args):
    return WelderPanel(*args)


class WelderPanel(ScreenPanel):
    distances = ['.1', '.5', '1', '5', '10', '25', '50']
    distance = distances[-2]

    def __init__(self, screen, title, back=True):
        super().__init__(screen, title, back)
        self.settings = {}
        self.menu = ['move_menu']

    def initialize(self, panel_name):
        grid = self._gtk.HomogeneousGrid()

        self.labels['gastest'] = self._gtk.ButtonImage("fan", "Gas Test", "color4")
        self.labels['gastest'].connect("pressed", self.run_gcode_macro, "MEGMEET_GAS_START")
        self.labels['gastest'].connect("released", self.run_gcode_macro, "MEGMEET_GAS_END")

        self.labels['locatingon'] = self._gtk.ButtonImage("move", "Move", "color2")
        self.labels['locatingon'].connect("clicked", self.menu_item_clicked, "move", {
            "name": "Move",
            "panel": "move"
        })
        #self.labels['locatingon'] = self._gtk.ButtonImage("custom-script", "Locating On", "color2")
        #self.labels['locatingon'].connect("clicked", self.run_gcode_macro, "MEGMEET_LOCATING_ENABLE")
        self.labels['locatingoff'] = self._gtk.ButtonImage("home", "Zero Axes", "color2")
        self.labels['locatingoff'].connect("clicked", self.run_gcode_macro, "SET_KINEMATIC_POSITION X=0 Y=0 Z=0")
        #self.labels['locatingoff'] = self._gtk.ButtonImage("custom-script", "Locating Off", "color2")
        #self.labels['locatingoff'].connect("clicked", self.run_gcode_macro, "MEGMEET_LOCATING_DISABLE")

        self.labels['arctest'] = self._gtk.ButtonImage("load", "Arc Test", "color1")        
        self.labels['arctest'].connect("pressed", self.run_gcode_macro, "MEGMEET_ARC_START")
        self.labels['arctest'].connect("released", self.run_gcode_macro, "MEGMEET_ARC_END")

        self.labels['feed-back'] = self._gtk.ButtonImage("arrow-up", "Feed Reverse", "color3")
        self.labels['feed-back'].connect("pressed", self.run_gcode_macro, "MEGMEET_WIRE_REVERSE_START")
        self.labels['feed-back'].connect("released", self.run_gcode_macro, "MEGMEET_WIRE_REVERSE_END")

        self.labels['feed-forward'] = self._gtk.ButtonImage("arrow-down", "Feed Forward", "color3")
        self.labels['feed-forward'].connect("pressed", self.run_gcode_macro, "MEGMEET_WIRE_FORWARD_START")
        self.labels['feed-forward'].connect("released", self.run_gcode_macro, "MEGMEET_WIRE_FORWARD_END")

        self.labels['motors-off'] = self._gtk.ButtonImage("motor-off", _("Disable Motors"), "color4")
        script = {"script": "M18"}
        self.labels['motors-off'].connect("clicked", self._screen._confirm_send_action,
                                          _("Are you sure you wish to disable motors?"),
                                          "printer.gcode.script", script)

        grid.attach(self.labels['feed-forward'], 2, 1, 1, 1)
        grid.attach(self.labels['gastest'], 0, 1, 1, 1)
        #grid.attach(self.labels['feed-forward'], 0, 1, 1, 1)
        #grid.attach(self.labels['gastest'], 2, 1, 1, 1)
        grid.attach(self.labels['locatingon'], 1, 0, 1, 1)
        grid.attach(self.labels['locatingoff'], 1, 1, 1, 1)

        grid.attach(self.labels['arctest'], 0, 0, 1, 1)
        grid.attach(self.labels['feed-back'], 2, 0, 1, 1)

        self.labels['weld_current'] = Gtk.Label("")
        self.labels['weld_voltage'] = Gtk.Label("")
        self.labels['weld_error'] = Gtk.Label("")

        adjust = self._gtk.ButtonImage("refresh", "Error Reset", "color1")
        adjust.connect("pressed", self.run_gcode_macro, "MEGMEET_ERROR_RESET")

        bottomgrid = self._gtk.HomogeneousGrid()
        bottomgrid.set_direction(Gtk.TextDirection.LTR)
        bottomgrid.attach(self.labels['weld_current'], 0, 0, 1, 1)
        bottomgrid.attach(self.labels['weld_voltage'], 1, 0, 1, 1)
        bottomgrid.attach(self.labels['weld_error'], 2, 0, 1, 1)        
        bottomgrid.attach(adjust, 3, 0, 1, 2)

        self.labels['move_menu'] = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.labels['move_menu'].set_vexpand(True)
        self.labels['move_menu'].pack_start(grid, True, True, 0)
        self.labels['move_menu'].pack_start(bottomgrid, True, True, 0)        

        self.content.add(self.labels['move_menu'])

        printer_cfg = self._printer.get_config_section("printer")
        # The max_velocity parameter is not optional in klipper config.
        max_velocity = int(float(printer_cfg["max_velocity"]))

    def process_update(self, action, data):
        if action != "notify_status_update":
            return

        #homed_axes = self._screen.printer.get_stat("toolhead", "homed_axes")
        #if homed_axes == "xyz":
        #    if "gcode_move" in data and "gcode_position" in data["gcode_move"]:
        #        self.labels['pos_x'].set_text(f"X: {data['gcode_move']['gcode_position'][0]:.2f}")


    def run_gcode_macro(self, widget, macro):
        self._screen._ws.klippy.gcode_script(macro)

    def back(self):
        if len(self.menu) > 1:
            self.unload_menu()
            return True
        return False
