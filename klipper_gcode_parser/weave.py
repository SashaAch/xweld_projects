##!/usr/bin/python3
import time

from gcode_split_layer import split_gcode
from add_missing_points import Missing_points
from add_in_gcode_turn_on_off_arc import add_arc_in_gcode_file

from progress.bar import FillingSquaresBar
import numpy as np
import re
import glob
import os
import sys
import json
from natsort import natsorted


class Weave(Missing_points):
	def __init__(self, gcode_path: str, weave_param: str, output_weave_folder_path: str ='/home/klipper/printer_data/gcodes/output_weave',
	             output_split_folder_path:str='/home/klipper/printer_data/gcodes/output_split'):
		
		self.gcode_file = None
		self.gcode_path = gcode_path
		#self.gcode_path = f'/home/klipper/gcode_files/{gcode_path}'

		self.weave_param = weave_param
		self.output_weave_folder_path = output_weave_folder_path
		self.output_split_folder_path = output_split_folder_path

		self.weave_pattern, self.weave_distance, \
		self.weave_amplitude, self.weave_angle, self.weave_pause_list = self.get_weave_param(self.weave_param)
		self.len_weave_pattern = len(self.weave_pattern)
		self.segments_length = self.weave_distance / self.len_weave_pattern
		super().__init__(self.segments_length)

	def add_weave_all_layers(self):

		if not os.path.exists(self.output_weave_folder_path):
			os.makedirs(self.output_weave_folder_path)

		split_gcode(self.gcode_path, self.output_split_folder_path)  # исходный файл делиться по слоям

		files = glob.glob(f'{self.output_weave_folder_path}/*') # все файлы из папки output_weave_folder_path удаляются
		for f in files:
			os.remove(f)

		path_layer_list = natsorted(glob.glob(f'{self.output_split_folder_path}/layer *[0-9].gcode'))
		new_path = [path.replace(rf'{self.output_split_folder_path}', '') for path in path_layer_list]

		progress_bar = FillingSquaresBar('Add Weave', max=len(path_layer_list))
		with open(f'{self.output_weave_folder_path}/description.gcode', 'w') as description_weave, \
				open(f'{self.output_split_folder_path}/description.gcode', 'r') as description_split:
			description_weave.writelines(description_split.read())

			for gcode_file_index in range(len(path_layer_list)): # основной цикл weave
				progress_bar.next()

				with open(path_layer_list[gcode_file_index], 'r+') as gcode_file:
					gcode_read = gcode_file.read()
					gcode_with_weave_points = self.add_weave_in_gcode(gcode_read, path_layer_list[gcode_file_index],
					                                                  gcode_file_index)
					# g0_string = 'G0\nG0\n'
					# gcode_with_weave_points.insert(0, g0_string)
					description_split.seek(0)
					description_readlines = description_split.readlines()[:18]
					description_string = ''.join(i for i in description_readlines) + 'G28 X0 Y0\n'
					gcode_with_weave_points.insert(0, description_string)

					gcode_file.seek(0)
					gcode_readlines = gcode_file.readlines()
					gcode_readlines.insert(0, description_string)
					gcode_file.seek(0)
					gcode_file.writelines(gcode_readlines)

				with open(f'{self.output_weave_folder_path}/{new_path[gcode_file_index]}', 'w') as weave_file:
					weave_file.writelines(gcode_with_weave_points)

		with open(f'{self.output_weave_folder_path}/layer_end.gcode', 'w') as end_weave, \
			open(f'{self.output_split_folder_path}/layer_end.gcode', 'r') as end_split:

			end_weave.writelines(end_split.read())

		progress_bar.finish()

		# Для отладки, всё собирается в 1 файл#########################
		# p = 'output_weave'
		# if not os.path.exists('weave'):
		# 	os.makedirs('weave')
		#
		# path_layer_list = glob.glob(f'{p}/*.gcode')
		#
		# for i in range(len(path_layer_list) -2 ):
		# 	with open(f'{self.output_split_folder_path}/description.gcode', 'a') as weave_file, open(f'{p}/layer'
		# 		f' {i + 1}.gcode') as layer:
		# 		weave_file.writelines(layer)
		#
		# with open(f'{self.output_split_folder_path}/description.gcode', 'a') as weave_file, \
		# 		open(f'output_split/layer_end.gcode') as end_layer:
		# 	weave_file.writelines(end_layer)
		#
		# with open(f'{self.output_split_folder_path}/description.gcode', 'r') as weave_file, \
		# 		open(f'weave/Weaved {self.gcode_path.partition("/")[2]}.gcode', 'w') as fin:
		# 	fin.writelines(weave_file)
		###############################################################

		print('Add weave done!')

	def get_weave_param(self, param_name: str) -> (tuple, float or int, float or int, int):

		with open('/home/klipper/xweld_projects/gcode_parser/weave_config.json', 'r') as json_file:
			weave_param = json.load(json_file)

		return tuple(weave_param[f'{param_name}']['weave_pattern']), \
		       weave_param[f'{param_name}']['weave_distance'], \
		       weave_param[f'{param_name}']['weave_amplitude'], \
		       weave_param[f'{param_name}']['weave_angle'], \
		       [0, 0, 0, 0] if not 'weave_pause' in weave_param[f'{param_name}'] else \
			       weave_param[f'{param_name}']['weave_pause']

	def get_parameters_gcode(self, gcode: str = None, input_gcode_path: str = None) -> (list, str):
		return super().get_params_gcode(gcode, input_gcode_path)

	def add_missing_points(self, gcode, input_gcode_path: str = None, id=None):
		return super().add_missing_points_in_list(gcode, input_gcode_path, id)

	def add_weave_in_gcode(self, gcode, input_gcode_path: str = None, id=None) -> list:

		path = natsorted(glob.glob(f'{self.output_split_folder_path}/*'))

		gcode_list = ''.join(self.add_missing_points(gcode, input_gcode_path, path[id]))

		params, gcode_lines = self.get_parameters_gcode(gcode_list, input_gcode_path)

		# print(*params, sep='\n')

		list_with_weave = []
		pars_lines = gcode_lines.splitlines()

		# print(pars_lines)
		i = 0
		line = 0
		# print(len(params))
		# print(len(pars_lines))
		# for line in range(len(pars_lines)):
		while True:
			# print(line == len(pars_lines))
			if i > len(params) - 1:
				list_with_weave.append(f'{pars_lines[line]}\n')
				list_with_weave.append(f'G28\nG28\n')
				# list_with_weave.append(f'{pars_lines[-1]}\n') # если есть функция add_arc_in_gcode_file() в weave()
				break

			code_start = params[i][0]
			code_end_lines = params[i+1:i + self.len_weave_pattern + 1]
			code_end = list(map(lambda x: x[0], code_end_lines))

			start = params[i][1], params[i][2], params[i][3]

			end_lines = params[i+1:i + self.len_weave_pattern + 1]
			end = list(map(lambda x: (x[1], x[2], x[3]), end_lines))

			start_extrusion = params[i][4]
			end_extrusion = list(map(lambda x: (x[-2]), end_lines))

			start_distance = params[i][5]

			start_condition = start_extrusion == None
			# end_condition = end_extrusion[0] == None
			end_condition = list(map(lambda x: x == None, end_extrusion))
			end_condition.append(start_condition)
			start_end_extrusion_condition = any(end_condition)

			if 'LAYER' in pars_lines[line]:
				print(pars_lines[line], 'weave')
				list_with_weave.append(f'{pars_lines[line]}\n')

			if re.findall(rf'{code_start}', pars_lines[line]):
				if 'G92' in code_end and code_start == 'G1':
					# list_with_weave.append(f'{pars_lines[line]}\n')
					line += 1
					i += 1
					continue

				if start_end_extrusion_condition:
					if f'{pars_lines[line]}\n' not in list_with_weave:
						list_with_weave.append(f'{pars_lines[line]}\n')
					# print(pars_lines[line])
					i += 1
					continue
				elif start_extrusion > 0:

					# self.weave_pause(list_with_weave, self.weave_pause_list[0])
					# line = line.partition('E')[0] + 'E' + str(start_extrusion) + '; ' + str(start_distance)
					# list_with_weave.append(f'{line}\n')
					# self.weave_pause(list_with_weave, self.weave_pause_list[1])

					self.weave_pause(list_with_weave, self.weave_pause_list[2])
					weave_p = self.weave_point(code_start, start, end, start_extrusion, end_extrusion,
					                           self.weave_amplitude, self.weave_angle, self.weave_pattern,
					                           pars_lines[line])
					list_with_weave.extend(weave_p)
					self.weave_pause(list_with_weave, self.weave_pause_list[3])

				else:
					list_with_weave.append(f'{pars_lines[line]}\n')
					# print(pars_lines[line])
				# print(code_start) if 'G92' in code_start else None
				# print(code_start) if 'G92' in code_end else None
				# print(code_end) if 'G92' in code_end else None
				i += self.len_weave_pattern
			else:
				if 'F9000' in pars_lines[line]:
					# pars_lines[line] = pars_lines[line].partition('E')[0]
					list_with_weave.append(f'{pars_lines[line]}\n')



					# print(pars_lines[line][:2])
				# list_with_weave.append(f'{pars_lines[line]}\n')
				# print(pars_lines[line])
				# time.sleep(1)
				# print('i', i)
				# print('line', line)

				line += 1

		# print(list_with_weave)
		# print(*set_test)
		return list_with_weave

	def weave_pause(self, list_with_weave:list, weave_pause_seconds: int) -> list:
		list_with_weave.append(f'G4 P{weave_pause_seconds}\n') if weave_pause_seconds > 0 else None
		return list_with_weave


	def weave_point(self, code: str, start: tuple, end: list, start_extrusion: float, end_extrusion: list,
	                weave_amplitude: int, weave_angle: int, weave_pattern:tuple, idx) -> list:

		points = np.array((start, *end))
		extrusion = [start_extrusion, *end_extrusion]
		weave_points = []
		for i in range(1, len(points)):
			vector = points[i] - points[i-1]
			uni_vector = self.unit_vector(vector, points[0])
			rotation_vector = self.z_rotation(uni_vector, weave_angle)
			rotation_vector *= weave_amplitude / 2
			weave_point = (points[i-1] + rotation_vector * weave_pattern[i-1])
			weave_extrusion = (extrusion[i] + extrusion[i-1]) / 2
			weave_point_length = np.sqrt(uni_vector.dot(uni_vector))

			weave_p = (f'{code}'
		         f'{" X" + str(round(weave_point[0], 4)) if weave_point[0] > weave_amplitude or weave_point[0] < -weave_amplitude else f" X{start[0]}" if start[0] > 0 else ""}'
		         f'{" Y" + str(round(weave_point[1], 4)) if weave_point[1] > weave_amplitude or weave_point[1] < -weave_amplitude else f" Y{start[1]}" if start[1] > 0 else ""}'
		         f'{" Z" + str(round(start[2], 4)) if start[2] > 0 or start[2] < 0 else  ""}'
		         f'{" E" + str(round(weave_extrusion, 8))}; '
		         f'{(str(weave_point_length))}\n')

			weave_points.append(weave_p)

		return weave_points


		# vector = end_points - start_points
		# uni_vector = self.unit_vector(vector)
		# uni_vector = self.z_rotation(uni_vector, weave_angle)
		# uni_vector *= weave_amplitude
		# weave_point = start_points + uni_vector
		# weave_point_length = np.sqrt(uni_vector.dot(uni_vector))
		#
		# weave_extrusion = (end_extrusion + start_extrusion) / 2
		#
		# point = (f'{code}'
		#          f'{" X" + str(round(weave_point[0], 4)) if weave_point[0] > weave_amplitude or weave_point[0] < -weave_amplitude else f" X{start[0]}" if start[0] > 0 else ""}'
		#          f'{" Y" + str(round(weave_point[1], 4)) if weave_point[1] > weave_amplitude or weave_point[1] < -weave_amplitude else f" Y{start[1]}" if start[1] > 0 else ""}'
		#          f'{" Z" + str(round(start[2], 4)) if start[2] > 0 or start[2] < 0 else  ""}'
		#          f'{" E" + str(round(weave_extrusion, 8))}; '
		#          f'{(str(weave_point_length))}\n')
		# return point

	def unit_vector(self, vector, indx):
		""" Returns the unit vector of the vector."""
		# print(indx, 'vec')
		# print(indx)
		return vector / np.sqrt(vector.dot(vector))

	# def angle_between(v1, v2):
	#     """Finds angle between two vectors"""
	#     v1_u = unit_vector(v1)
	#     v2_u = unit_vector(v2)
	#     return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
	#
	# def x_rotation(vector,theta):
	#     """Rotates 3-D vector around x-axis"""
	#     theta = (theta * np.pi) / 180
	#     R = np.array([[1,0,0],[0,np.cos(theta),-np.sin(theta)],[0, np.sin(theta), np.cos(theta)]])
	#     return np.dot(R,vector)
	#
	# def y_rotation(vector,theta):
	#     """Rotates 3-D vector around y-axis"""
	#     theta = (theta * np.pi) / 180
	#     R = np.array([[np.cos(theta),0,np.sin(theta)],[0,1,0],[-np.sin(theta), 0, np.cos(theta)]])
	#     return np.dot(R,vector)

	def z_rotation(self, vector, theta):
		"""Rotates 3-D vector around z-axis"""
		theta = (theta * np.pi) / 180
		R = np.array([[np.cos(theta), -np.sin(theta), 0], [np.sin(theta), np.cos(theta), 0], [0, 0, 1]])
		return np.dot(R, vector)


def call_pars_func(file_name='Calibration_cube.gcode', weave_param='weave_1',
                   output_weave_folder_path: str =r'/home/klipper/printer_data/gcodes/output_weave',
	             output_split_folder_path:str=r'/home/klipper/printer_data/gcodes/output_split'):
	print(file_name, 'splitting...')
	Weave(file_name, weave_param, output_weave_folder_path, output_split_folder_path).add_weave_all_layers()


def main():
	# убрать G92, пересчитать экструзию при разделении на слои
	if len(sys.argv) > 1:
		call_pars_func(sys.argv[1], sys.argv[2])
	# else:
	# 	file_name = 'gcode_xweld/obtekatel_v6_cura_5.2.1.gcode'
	# 	# file_name = 'gcode_test/Calibration_cube.gcode'
	# 	weave_param = 'weave_2'
	# 	output_weave_folder_path: str = 'output_weave'
	# 	output_split_folder_path: str = 'output_split'
	# 	Weave(file_name, weave_param, output_weave_folder_path, output_split_folder_path).add_weave_all_layers()

if __name__ == '__main__':
	main()
