from gcodeparser import GcodeParser
import re
import glob
import os
import timeit
"""
Добавляет ARC_ON или ARC_OFF, в зависимости от изменения экструзии и пройденного пути
"""

def get_params_gcode_list(path) -> (list, str):
	with open(path, 'r') as gcode_file:
		gcode_lines = gcode_file.read()
		pars = GcodeParser(gcode_lines)

		params_gcode = [(''.join(map(str, pars.lines[i].command)),
		                 pars.lines[i].get_param('E'),
		                 float(pars.lines[i].comment)) for i in range(len(pars.lines))
		                ]

	return params_gcode, gcode_lines


def add_arc_in_gcode(params: list, gcode_lines: str, dist_to_skip_arc_off: float = 0.0,
                     accumulate_var_condition:float=0) -> list:
	list_with_arc = []
	pars_lines = gcode_lines.splitlines()
	i = 1
	arc_flag = False
	accumulate_flag = False
	accumulate = 0.0

	for line in pars_lines:
		if i == len(params):
			list_with_arc.append(f'{line}\n')
			list_with_arc.append('ARC_OFF\n')
			break

		param1_E = params[i - 1][1]
		param2_E = params[i][1]
		code = params[i - 1][0]
		distance = params[i - 1][2]
		distance_condition = dist_to_skip_arc_off <= distance
		arc_on_condition = isinstance(param1_E, float) and isinstance(param2_E, float) and (
				param2_E - param1_E) > 0 and not arc_flag

		if re.findall(rf'{code}', line):
			if param1_E is None:

				accumulate += distance
				if accumulate > accumulate_var_condition:
					accumulate_flag = True

				if (arc_flag and distance_condition or accumulate_flag):
					list_with_arc.append('ARC_OFF\n')
					list_with_arc.append(f'{line}\n')
					arc_flag = False
					accumulate_flag = False
					accumulate = 0.0
				else:
					list_with_arc.append(f'{line}\n')
				i += 1
				continue
			elif arc_on_condition:
				list_with_arc.append('ARC_ON\n')
				list_with_arc.append(f'{line}\n')
				arc_flag = True
				accumulate = 0.0
			else:
				list_with_arc.append(f'{line}\n')
			i += 1
		else:
			list_with_arc.append(f'{line}\n')

	return list_with_arc


def add_arc_in_gcode_file(output_path:str, distance_of_rupture_remove: int, accumulate_var_condition: int) -> None:
	# files = glob.glob('output_gcode_with_arc/*')
	# for f in files:
	# 	os.remove(f)

	path_layer_list = glob.glob(f'{output_path}/layer *[0-9].gcode')
	distance_of_rupture_remove = distance_of_rupture_remove
	accumulate_var_condition = 100

	new_path = [path.replace(rf'{output_path}', '') for path in path_layer_list]

	for i in range(len(path_layer_list)):
		params, gcode = get_params_gcode_list(path_layer_list[i])
		list_with_arc = add_arc_in_gcode(params, gcode, distance_of_rupture_remove, accumulate_var_condition)
		with open(f'{output_path}/{new_path[i]}', 'w') as test:
			test.writelines(list_with_arc)

def main():
	add_arc_in_gcode_file('output', 10, 200)


if __name__ == '__main__':
	main()
