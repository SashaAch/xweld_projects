import pandas as pd
from gcodeparser import GcodeParser
import re
import glob
import time
from progress.bar import FillingSquaresBar

"""
Вычисляет длину дистанции для каждой точки в каждом слое gcode (в папке output) или отдельно для выбранного файла
"""


class Vector_lenght:
	def __init__(self, folder_split_output:str='output'):
		self.output_path = folder_split_output

	def calculate_gcode_distance(self, gcode_lines:str=None, input_path:str=None) -> list:

		dist_list, command_list, gcode_split = self.get_data_frame(input_path, gcode_lines)

		comment_list = list(zip(command_list, dist_list))
		gcode_splitlines = self.del_comm(gcode_split)

		gcode_with_distance_list = []
		# print(gcode_splitlines[0])
		i = 0
		for line in gcode_splitlines:
			distance = comment_list[i][1]
			code = comment_list[i][0]
			# print(code, line)
			if re.findall(rf'{code}', line) and distance > 0.0:
				line = line.partition(';')[0]
				gcode_with_distance_list.append(f'{line}; {distance}\n')
				i += 1
			elif distance == 0.0:
				i += 1
				continue
			else:
				gcode_with_distance_list.append(f'{line}\n')

		return gcode_with_distance_list

	def calculate_distance_all_split_files(self, input_path:str=None) -> None:
		path_layer_list = glob.glob(f'{self.output_path}/layer *[0-9].gcode')
		bar = FillingSquaresBar('Calculate distance', max=len(path_layer_list))
		for self.input_path in path_layer_list:
			bar.next()
			gcode_with_distance_list = self.calculate_gcode_distance()
			self.save_dist_in_file(input_path, gcode_with_distance_list)
		bar.finish()

	def get_data_frame(self, input_file: str, gcode_list:str= None) -> (list, list):
		with open(input_file, 'r') as file:
			if gcode_list is not None:
				self.gcode = gcode_list
			else:
				self.gcode = file.read()
			gcode_split = self.gcode.splitlines()

			pars_gcode_lines = GcodeParser(self.gcode).lines

			params = ({
				"X": 0 if pars_gcode_lines[i].get_param('X') is None else pars_gcode_lines[i].get_param('X'),
				'Y': 0 if pars_gcode_lines[i].get_param('Y') is None else pars_gcode_lines[i].get_param('Y'),
				'Z': 0 if pars_gcode_lines[i].get_param('Z') is None else pars_gcode_lines[i].get_param('Z')}
				for i in range(len(pars_gcode_lines)))

			codes = (pars_gcode_lines[i].command for i in range(len(pars_gcode_lines)))
			codes = ({'command': ''.join(map(str, i))} for i in codes)

			df_params = pd.DataFrame(params)
			if len(df_params) > 0:
				df = pd.DataFrame(codes).join(df_params)
				# print(df)
				df_params = df_params.rename(columns={'X': 'X1', 'Y': 'Y1', 'Z': 'Z1'})
				# print(df_params)
				df_params = df_params.drop(index=[0])
				df_params = df_params.reset_index(drop=True)
				df = df.join(df_params, how='left').fillna(0)
				# print(df)
				df = df.eval('dist = ( (Z1-Z)**2 + (X1-X)**2 + (Y1-Y)**2 )**0.5')
				df = df.drop(['Z1', 'X1', 'Y1'], axis=1)
				# print(df)

				return df['dist'].tolist(), df['command'].tolist(), gcode_split
			else:
				raise IOError('Split layer error!')

	def save_dist_in_file(self, path: str, gcode_with_distance_list: list) -> None:
		with open(path, 'w') as f:
			f.writelines(gcode_with_distance_list)

	def del_comm(self, gcode_splitlines: list) -> list:
		first_pattern = re.compile(r'\bLAYER:\d+\b')
		for i in range(len(gcode_splitlines)):

			if first_pattern.findall(gcode_splitlines[i]):
				# print(gcode_splitlines[i], 'length')
				continue
			elif ';' in gcode_splitlines[i]:
				gcode_splitlines[i] = gcode_splitlines[i].partition(';')[0]
				# print(gcode_splitlines[i])

		list_del_com = list(filter(len, gcode_splitlines))

		return list_del_com


def main():
	vec_length = Vector_lenght('Calibration_cube.gcode').calculate_distance_all_split_files()
	print(vec_length)

if __name__ == '__main__':
	main()
