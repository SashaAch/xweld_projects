from gcodeparser import GcodeParser
import re
import math
import glob
import os
from progress.bar import FillingSquaresBar
from natsort import natsorted
import numpy as np

class Stretch:
	def __init__(self, path_input, output_stretch,  startA, endA, startB, endB):
		self.input_folder_path = path_input
		self.output_stretch_path = output_stretch
		self.startA = startA
		self.endA = endA
		self.startB = startB
		self.endB = endB


	def get_params_gcode(self, gcode) -> (list, str):
		with open(gcode, 'r') as gcode_file:
			gcode_lines = gcode_file.read()
			pars = GcodeParser(gcode_lines)

			params_gcode = [(''.join(map(str, pars.lines[i].command)),
			                 0 if pars.lines[i].get_param('X') is None else pars.lines[i].get_param('X'),
			                 0 if pars.lines[i].get_param('Y') is None else pars.lines[i].get_param('Y'),
			                 0 if pars.lines[i].get_param('Z') is None else pars.lines[i].get_param('Z'),
			                 pars.lines[i].get_param('E'),
			                 # float(pars.lines[i].comment),
			                 )
			                for i in range(len(pars.lines))
			                ]

			return params_gcode, gcode_lines

	def add_stretch_in_gcode(self, z_diff:int = 0) -> list:
		list_with_stretch = []
		pars_lines = self.gcode_lines.splitlines()

		i = 0

		for line in pars_lines:
			if i == len(self.params_gcode) - 1:
				code_start = self.params_gcode[i][0]
				start_point = self.params_gcode[i][1], self.params_gcode[i][2], self.params_gcode[i][3] + z_diff
				start_extrusion = self.params_gcode[i][4]

				stretch_point = self.alignPoint(start_point)
				stretch_line = self.set_full_stretch_point(code_start, stretch_point, start_extrusion, start_point)
				list_with_stretch.append(f'{stretch_line}')
				break

			code_start = self.params_gcode[i][0]
			start_point = self.params_gcode[i][1], self.params_gcode[i][2], self.params_gcode[i][3] + z_diff
			start_extrusion = self.params_gcode[i][4]

			if re.findall(rf'{code_start}', line):

				if code_start == 'G1' or code_start == 'G0':

					stretch_point = self.alignPoint(start_point)
					stretch_line = self.set_full_stretch_point(code_start, stretch_point, start_extrusion, start_point)
					list_with_stretch.append(f'{stretch_line}')
				else:
					list_with_stretch.append(f'{line}\n')
				i += 1
			else:
				list_with_stretch.append(f'{line}\n')

		return list_with_stretch

	def add_stretch_all_layers(self, z_diff:int = 0):

		if not os.path.exists(self.output_stretch_path):
			os.makedirs(self.output_stretch_path)

		files = glob.glob(f'{self.output_stretch_path}/*') # все файлы из папки output_weave_folder_path удаляются
		output_folder = self.input_folder_path.split('/')[-2]
		for f in files:
			os.remove(f)

		path_layer_list = natsorted(glob.glob(f'{self.input_folder_path}/layer *[0-9].gcode'))
		new_path = [path.partition(rf"{output_folder}")[2] for path in path_layer_list]

		progress_bar = FillingSquaresBar('Add Weave', max=len(path_layer_list))
		with open(f'{self.output_stretch_path}/description.gcode', 'w') as description_stretch, \
				open(f'{self.input_folder_path}/description.gcode', 'r') as description_input:
			description_stretch.writelines(description_input.read())

			for gcode_file_index in range(len(path_layer_list)): # основной цикл переноса
				progress_bar.next()

				self.params_gcode, self.gcode_lines = self.get_params_gcode(path_layer_list[gcode_file_index])

				gcode_stretch = self.add_stretch_in_gcode(z_diff)

				with open(f'{self.output_stretch_path}/{new_path[gcode_file_index]}', 'w') as stretch_file:
					stretch_file.writelines(gcode_stretch)

		with open(f'{self.output_stretch_path}/layer_end.gcode', 'w') as end_stretch, \
			open(f'{self.input_folder_path}/layer_end.gcode', 'r') as end_input:

			end_stretch.writelines(end_input.read())

		progress_bar.finish()

	@staticmethod
	def set_full_stretch_point(code: str, stretch_point:list, start_extrusion: float, start_point:tuple) -> str:

		point = (
				 f'{code}'
		         f'{" X" + str(round(stretch_point[0], 4))if start_point[0] > 0 or start_point[0] < 0 else ""}'
		         f'{" Y" + str(round(stretch_point[1], 4)) if  start_point[1] > 0 or start_point[1] < 0 else ""}'
		         f'{" Z" + str(round(stretch_point[2], 4)) if  start_point[2] > 0 or start_point[2] < 0 else ""}'
		         f'{" E" + str(round(start_extrusion, 4)) if start_extrusion is not None else ""}\n'
		         # f'{(str(weave_point_length))}\n'
		         )

		return point
	
	def calculateAlign(self) -> dict:
		
		vectorA = [self.endA[0] - self.startA[0], self.endA[1] - self.startA[1], self.endA[2] - self.startA[2]]
		vectorB = [self.endB[0] - self.startB[0], self.endB[1] - self.startB[1], self.endB[2] - self.startB[2]]
		lengthA = math.sqrt(vectorA[0] ** 2 + vectorA[1] ** 2 + vectorA[2] ** 2)
		lengthB = math.sqrt(vectorB[0] ** 2 + vectorB[1] ** 2 + vectorB[2] ** 2)
		vectorAngle = 2 * math.degrees(math.acos(
			(vectorA[0] * vectorB[0] + vectorA[1] * vectorB[1] + vectorA[2] * vectorB[2]) / (
					math.sqrt(vectorA[0] ** 2 + vectorA[1] ** 2 + vectorA[2] ** 2) * math.sqrt(
				vectorB[0] ** 2 + vectorB[1] ** 2 + vectorB[2] ** 2))))
		# print(vectorAngle)
		scale = lengthB / lengthA
		return dict(startPoint=self.startA, endPoint=self.startB, angle=vectorAngle, scale=scale)

	@staticmethod
	def rotateXY(point: list, angle) -> list:
		an = math.radians(angle)
		cs = math.cos(an)
		sn = math.sin(an)
		return [point[0] * cs - point[1] * sn, point[0] * sn + point[1] * cs, point[2]]

	def alignPoint(self, point:tuple) -> list:

		align = self.calculateAlign()
		
		shiftedPoint = [point[0] - align['startPoint'][0], point[1] - align['startPoint'][1],
		                point[2] - align['startPoint'][2]]

		scaledPoint = [shiftedPoint[0] * align['scale'], shiftedPoint[1] * align['scale'],
		               shiftedPoint[2] * align['scale']]

		turnedPoint = self.rotateXY(scaledPoint, align['angle'])
		return [turnedPoint[0] + align['endPoint'][0], turnedPoint[1] + align['endPoint'][1],
		        turnedPoint[2] + align['endPoint'][2]]


def main():
	# Stretch('D:/Projects/xweld_projects/gcodeparser/output_weave/',
	# r'D:\Projects\xweld_projects\align\output_stretch',
	#     [312.320, 204.705, 0], [607.111, 499.496, 0], [0, 0, 0], [303, 304, 0]).add_stretch_all_layers(z_diff=-25)
	with open('D:/Projects/xweld_projects/gcodeparser/output_split/layer 1.gcode', 'r') as first_layer:

		l = first_layer.readlines()
		first_coord, second_coord = l[5], l[6]
		first_coord = first_coord.partition('G1')[-1].partition('Z')[0]
		second_coord = second_coord.partition('G1')[-1].partition('Z')[0]

		first_coord = list(map(lambda x:float(x[1:]), first_coord.split()))
		second_coord = list(map(lambda x: float(x[1:]), second_coord.split()))
		first_coord.append(0)
		second_coord.append(0)

		dif_x = first_coord[0] - second_coord[0]
		dif_y = first_coord[1] - second_coord[1]

		print(first_coord, second_coord)
		print(dif_x)
		print(dif_y)

		def transpose_coord(first_coord, new_x, new_y):
			x, y, z = first_coord
			return new_x, new_y, z

		new_x, new_y, new_z = transpose_coord(first_coord, 0, 0)

		def rotate_coord(x,y,z, angle):
			rotate_num = 0
			x, y, z = x, y, z
			x, y = x-dif_x-dif_y, y - 10

			return x, y, z

		rotate_x, rotate_y, rotate_z = rotate_coord(new_x, new_y, new_z, 0)
		print(rotate_x)

	Stretch('D:/Projects/xweld_projects/gcodeparser/output_split/',
	        r'D:\Projects\xweld_projects\align\output_stretch/',
	        # X125.178 Y80.533 Z0
	        # X132.703 Y73.007 Z0

	        first_coord, (first_coord[0], first_coord[1]+10, 0),
	        (new_x, new_y, new_z), (rotate_x, rotate_y,rotate_z)).add_stretch_all_layers(z_diff=0)


if __name__ == '__main__':
	main()
